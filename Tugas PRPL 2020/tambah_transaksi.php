<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$pembeli = mysqli_query($koneksi, "SELECT * FROM pembeli");
	$menu = mysqli_query($koneksi, "SELECT * FROM menu");
?>

<div class="mt-3 ml-3 w-50">
	<h3>Tambah Transaksi</h3>

	<form method="post" action="proses_transaksi.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Kode Transaksi</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="Kode_Transaksi" placeholder="Masukkan Kode Transaksi">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<select class="form-control" name="pembeli">
					<?php foreach ($pembeli as $value): ?>
						<option value="<?=$value['ID_Pembeli'];?>">
							<?=$value['ID_Pembeli'];?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Menu</label>
			<div class="col-sm-10">
				<select class="form-control" name="menu">
					<?php foreach ($menu as $value): ?>
						<option value="<?=$value['ID_Menu'];?>">
							<?=$value['ID_Menu'];?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>


		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Tanggal Transaksi</label>
			<div class="col-sm-10">
				<input type="date" class="form-control" id="formGroupExampleInput" name="Tgl_Transaksi" placeholder="Masukkan Tanggal Transaksi">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Total Harga</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="Total_Harga" placeholder="Masukkan total Harga">
			</div>
		</div>
		<button name="tambah" class ="badge badge-primary">Tambahkan</button>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>



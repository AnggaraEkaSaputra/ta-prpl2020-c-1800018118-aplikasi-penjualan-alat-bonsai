<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';
?>

<div class="mt-3 ml-3 w-50">
	<h3>Tambah Menu</h3>

	<form method="post" action="proses_produk.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">ID Menu</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="ID_Menu" placeholder="Masukkan Id Produk">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jenis Menu</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="Jenis_Menu" placeholder="Masukkan menu">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Harga</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="Harga" placeholder="Masukkan harga">
			</div>
		</div>
		<button name="tambah" class ="badge badge-primary">Tambahkan</button>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>
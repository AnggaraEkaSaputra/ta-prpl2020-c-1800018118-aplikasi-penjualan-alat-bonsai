<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$transaksi = mysqli_query($koneksi, "SELECT transaksi.Kode_Transaksi, transaksi.ID_Pembeli, transaksi.ID_Menu, Menu.Jenis_Menu, pembeli.Jumlah_Beli, Menu.Harga, transaksi.Tgl_Transaksi, transaksi.Total_Harga  FROM transaksi JOIN pembeli ON transaksi.ID_Pembeli = pembeli.ID_Pembeli JOIN menu ON transaksi.ID_Menu = Menu.ID_Menu");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT transaksi.Kode_Transaksi, transaksi.ID_Pembeli, transaksi.ID_Menu, Menu.Jenis_Menu, pembeli.Jumlah_Beli, Menu.Harga, transaksi.Tgl_Transaksi, transaksi.Total_Harga  FROM transaksi JOIN pembeli ON transaksi.ID_Pembeli = pembeli.ID_Pembeli JOIN menu ON transaksi.ID_Menu = Menu.ID_Menu where Kode_Transaksi like '%$key%'");
	}
	else {
		$cari = $transaksi;
	}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-8">
			<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-3">
				<form method="get" class="ml-3 mt-3">
					<label for="formGroupExampleInput">Pencarian</label>
					<div class="input-group mb-3 w-100">
					    <input type="text" class="form-control" name="cari" placeholder="Pencarian">
					    <div class="input-group-apend">
						    <input type="submit"class="ml-3 w-100 h-100">
						</div>
					</div>
				</form>

				<table class="table table-bordered w-100 p-3 ml-3 mt-5">
					<thead class="bg-light">
						<tr>
							<th scope="col">Kode_Transaksi</th>
							<th scope="col">ID_Pembeli</th>
							<th scope="col">ID_Menu</th>
							<th scope="col">Jenis_Menu</th>
							<th scope="col">Jumlah_Beli</th>
							<th scope="col">Tgl_Transaksi</th>
							<th scope="col">Total_Harga</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cari as $value):?>
						<tr>
							<th scope="row"><?php echo $value['Kode_Transaksi']; ?></th>
							<td><?php echo $value['ID_Pembeli']; ?></td>
							<td><?php echo $value['ID_Menu']; ?></td>
							<td><?php echo $value['Jenis_Menu']; ?></td>
							<td><?php echo $value['Jumlah_Beli']; ?></td>
							<td><?php echo $value['Tgl_Transaksi']; ?></td>
							<td><?php echo $value['Total_Harga']; ?></td>
							<td>
								<a href="edit_transaksi.php?id=<?php echo $value['Kode_Transaksi'] ?>" class ="badge badge-warning">edit</a>
								<a href="hapus_transaksi.php?id=<?php echo $value['Kode_Transaksi'] ?>" class ="badge badge-danger">hapus</a>
								<a href="tambah_transaksi.php" class ="badge badge-primary">Tambah Data</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</main>
		</div>
		<div class="col-4 mt-3">
			<div class="p-4 mb-3 mt-3 bg-light rounded-0 h-15">
				<h5>INFO DATA TRANSAKSI</h5>

				<table class="mt-1">
					<tr>
						<td>Total Data</td>
						<td scope="row">:</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>

			</div>
		</div>
	</div>
</div>
<?php 
	
	include 'layout/footer.php';

?>
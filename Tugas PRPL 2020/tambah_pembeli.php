<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';
	$petugas = mysqli_query($koneksi, "SELECT * FROM petugas");
?>

<div class="mt-3 ml-3 w-50">
	<h3>Tambah pembeli</h3>

	<form method="post" action="proses_pembeli.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="ID_Pembeli" placeholder="Masukkan Id Pembeli">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jumlah Beli</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="Jumlah_Beli" placeholder="Masukkan Jumlah Beli">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">ID Petugas</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="ID_Petugas" placeholder="Masukkan ID Petugas">
			</div>
		</div>
		<button name="tambah" class ="badge badge-primary">Tambahkan</button>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>
<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$menu = mysqli_query($koneksi, "SELECT * FROM menu");

	//$cari = $produk;

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT * FROM menu where Jenis_Menu like '%$key%'");
	}

/*	else if (isset($_GET['minimal'], $_GET['maksimal'])) {
		$min = $_GET['minimal'];
		$maks = $_GET['maksimal'];

		$cari = mysqli_query($koneksi, "SELECT * FROM produk where harga >= '$min' && harga <= '$maks'");
	}
	if (isset($_GET['cari'], $_GET['minimal'], $_GET['maksimal'])) {
		$key = $_GET['cari'];
		$min = $_GET['minimal'];
		$maks = $_GET['maksimal'];



		$cari = mysqli_query($koneksi, "SELECT * FROM produk where merk like '%$key%' && harga >= '$min' && harga <= '$maks'");
	}*/
	else {
		$cari = $menu;
	}
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-8">
			<main role="main" class="col-md-9 col-lg-12 px-3">
				<form method="get" class="ml-3 mt-3">
					<label for="formGroupExampleInput">Pencarian</label>
					<div class="input-group mb-3 w-100">
					    <input type="text" class="form-control" name="cari" placeholder="Pencarian">
					    <div class="input-group-apend">
						    <input type="submit"class="ml-3 w-100 h-100">
						</div>
					</div>
					
				<!--	<label class="mt-3">Sort By Time</label></br>
					    <input type="radio" name="gender" value="terbaru" class="">
					  	<label class="ml-3">terbaru</label>
					  	<input type="radio" name="gender" value="terlama" class="ml-3">
					  	<label class="ml-3">terlama</label>
					</br>-->

				<!--	<label class="mt-3">Sort By Harga</label></br>
						<div class="input-group mb-3 w-50">
						    <input type="text" class="form-control" name="minimal" placeholder="minimal">
						    
						    <input type="text" class="form-control ml-3" name="maksimal" placeholder="maksimal">
			
						</div>

					<div class="mt-4">
						<input type="submit" name="submit" class="h-100">
					</div>-->

				</form>

				<table class="table table-bordered w-100 p-3 ml-3 mt-5">
					<thead class="bg-light">
						<tr>
							<th scope="col">ID_Menu</th>
							<th scope="col">Barang</th>
							<th scope="col">Harga</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cari as $value):?>
						<tr>
							<th scope="row"><?php echo $value['ID_Menu']; ?></th>
							<td><?php echo $value['Jenis_Menu']; ?></td>
							<td><?php echo $value['Harga']; ?></td>
							<td>
								<a href="edit_produk.php?id=<?php echo $value['ID_Menu'] ?> " class ="badge badge-warning">edit</a>
								<a href="hapus_produk.php?id=<?php echo $value['ID_Menu'] ?>" class ="badge badge-danger">Hapus</a>
								<a href="tambah_produk.php" class ="badge badge-primary">Tambah Data</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</main>
		</div>
		<div class="col-4 mt-3">
			<div class="p-4 mb-3 mt-3 bg-light rounded-0 h-15">
				<h5>INFO DATA MENU</h5>

				<table class="mt-1">
					<tr>
						<td>Total Data</td>
						<td scope="row">:</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>

			</div>
		</div>
	</div>
</div>

<?php 
	
	include 'layout/footer.php';

?>
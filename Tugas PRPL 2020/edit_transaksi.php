<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$id = $_GET['id'];
	$data = mysqli_query($koneksi, "SELECT * FROM transaksi where Kode_Transaksi = '$id'");
	/*$data = mysqli_query($koneksi, "SELECT transaksi.id_transaksi, transaksi.id_pembeli, pembeli.nama, transaksi.id_produk, produk.merk, produk.harga, transaksi.tgl_transaksi, transaksi.total_bayar, transaksi.jumlah_beli  FROM transaksi JOIN pembeli ON transaksi.id_pembeli = pembeli.id_pembeli JOIN produk ON transaksi.id_produk = produk.id_produk WHERE id_transaksi = '$id'");*/

	$pembeli = mysqli_query($koneksi, "SELECT * FROM pembeli");
	$menu = mysqli_query($koneksi, "SELECT * FROM menu");

	foreach($data as $value):

?>

<div class="mt-3 ml-3 w-50">
	<h3>Ubah Transaksi</h3>

	<form method="post" action="ubah_transaksi.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Kode Transaksi</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="Kode_Transaksi" value="<?php echo $value['Kode_Transaksi'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">ID Pembeli</label>
			<div class="col-sm-10">
				<select class="form-control" name="ID_Pembeli">
					<?php foreach ($pembeli as $valueee): ?>
						<option value="<?=$valueee['ID_Pembeli'];?>">
							<?=$valueee['ID_Pembeli'];?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Menu</label>
			<div class="col-sm-10">
				<select class="form-control" name="ID_Menu">
					<?php foreach ($menu as $valuee): ?>
						<option value="<?=$valuee['ID_Menu'];?>">
							<?=$valuee['ID_Menu'];?> - <?=$valuee['Jenis_Menu'];?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jumlah Beli</label>
			<div class="col-sm-10">
				<input type="text"class="form-control" id="formGroupExampleInput" name="Jumlah_Beli">
			</div>
		</div>

		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Tanggal Transaksi</label>
			<div class="col-sm-10">
				<!--<input type="text" name="tgl_transaksi" class="form-control">-->
				<input type="date" name="Tgl_Transaksi" class="form-control" value="<?php echo $value['Tgl_Transaksi'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Total Harga</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="Total_Harga" value="<?php echo $value['Total_Harga'] ?>">
			</div>
		</div>
		<button name="tambah">Ubah</button>
	</form>
</div>


<?php 
	endforeach;
	
	include 'layout/footer.php';

?>